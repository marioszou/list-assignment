import React from 'react';

import './Char.css'

const char = (props) => {
    // //another method to style the component using inline styling
    // const style = {
    //     display: 'inline-block',
    //     padding: '16px',
    //     margin: '16px',
    //     border: '1px solid black',
    //     textAlign: 'center'
    // }
    return (
        // <div style={style} className="Char"> // if we use the inline styling method
        <div className="Char">
            <p onClick={props.click}>{props.letter}</p>
        </div>
    )
};

export default char;